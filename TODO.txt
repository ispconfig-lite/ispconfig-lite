
---------------------------------------
- ISPConfig 3 ToDo list
---------------------------------------

Please feel free to edit this file, add new tasks,
remove done tasks or assign yourself to a task.

Installer
--------------------------------------

- Load and update system config from file into sql database during installation.
- Add a function to let a server join a existing installation.


Server
--------------------------------------

- Add a backend plugin to configure network card settings. The IP address settings
  are stored in the server_ip table.


Mail module
--------------------------------------

- Show mail statistics in the interface. The mail statistics are stored
  in the database table mail_traffic and are collected by the file
  server/cron_daily.php


DNS module
--------------------------------------

- Add some kind of wizard to create DNS records easily. The idea is to have some
  kind of template(s) to create all needed A and MX records defined in the template
  with one click and the user has just to enter IP, domain, nameserver and select the
  template that he wants to use.


Administration module
--------------------------------------

- Add a firewall configuration form. Any suggestions for a good firewall
  script that runs on many Linux distributions, or shall we stay with Bastille
  firewall that is used in ISPConfig 2?


Clients module
--------------------------------------


Sites (web) module
--------------------------------------

- Add a function to the Sites module to create SSL certificates or upload
  existing SSL certs and SSL chain files. It might be a good idea to add
  this as a new tab named "SSL" to the exiting domain settings form.

- Make sure that changes in the domain name do not break the configuration.


BE-Designer module
--------------------------------------

WARNING: Please do not use the BE Designer at the moment, the serializing function
of the module editor may break some of the existing modules.

- Add a language file editor.


Remoting framework
--------------------------------------


Interface
--------------------------------------

- Enhance the list function to allow sorting by column
- Enhance the paging in lists (e.g. like this: [1 2 3 4 ... 10])
- Use graphical Icons in the lists for on / off columns.
- Add a graphical delete button to the lists.


General tasks
--------------------------------------

- Add, extend or modify comments in PEAR syntax so that they can be read with phpdocumentor.

