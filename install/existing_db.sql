-- MySQL dump 10.13  Distrib 5.1.52, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: dbispconfig
-- ------------------------------------------------------
-- Server version	5.1.52

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `attempts_login`
--

LOCK TABLES `attempts_login` WRITE;
/*!40000 ALTER TABLE `attempts_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `attempts_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `client_template`
--

LOCK TABLES `client_template` WRITE;
/*!40000 ALTER TABLE `client_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` (`iso`, `name`, `printable_name`, `iso3`, `numcode`) VALUES ('AF','AFGHANISTAN','Afghanistan','AFG',4),('AL','ALBANIA','Albania','ALB',8),('DZ','ALGERIA','Algeria','DZA',12),('AS','AMERICAN SAMOA','American Samoa','ASM',16),('AD','ANDORRA','Andorra','AND',20),('AO','ANGOLA','Angola','AGO',24),('AI','ANGUILLA','Anguilla','AIA',660),('AQ','ANTARCTICA','Antarctica',NULL,NULL),('AG','ANTIGUA AND BARBUDA','Antigua and Barbuda','ATG',28),('AR','ARGENTINA','Argentina','ARG',32),('AM','ARMENIA','Armenia','ARM',51),('AW','ARUBA','Aruba','ABW',533),('AU','AUSTRALIA','Australia','AUS',36),('AT','AUSTRIA','Austria','AUT',40),('AZ','AZERBAIJAN','Azerbaijan','AZE',31),('BS','BAHAMAS','Bahamas','BHS',44),('BH','BAHRAIN','Bahrain','BHR',48),('BD','BANGLADESH','Bangladesh','BGD',50),('BB','BARBADOS','Barbados','BRB',52),('BY','BELARUS','Belarus','BLR',112),('BE','BELGIUM','Belgium','BEL',56),('BZ','BELIZE','Belize','BLZ',84),('BJ','BENIN','Benin','BEN',204),('BM','BERMUDA','Bermuda','BMU',60),('BT','BHUTAN','Bhutan','BTN',64),('BO','BOLIVIA','Bolivia','BOL',68),('BA','BOSNIA AND HERZEGOVINA','Bosnia and Herzegovina','BIH',70),('BW','BOTSWANA','Botswana','BWA',72),('BV','BOUVET ISLAND','Bouvet Island',NULL,NULL),('BR','BRAZIL','Brazil','BRA',76),('IO','BRITISH INDIAN OCEAN TERRITORY','British Indian Ocean Territory',NULL,NULL),('BN','BRUNEI DARUSSALAM','Brunei Darussalam','BRN',96),('BG','BULGARIA','Bulgaria','BGR',100),('BF','BURKINA FASO','Burkina Faso','BFA',854),('BI','BURUNDI','Burundi','BDI',108),('KH','CAMBODIA','Cambodia','KHM',116),('CM','CAMEROON','Cameroon','CMR',120),('CA','CANADA','Canada','CAN',124),('CV','CAPE VERDE','Cape Verde','CPV',132),('KY','CAYMAN ISLANDS','Cayman Islands','CYM',136),('CF','CENTRAL AFRICAN REPUBLIC','Central African Republic','CAF',140),('TD','CHAD','Chad','TCD',148),('CL','CHILE','Chile','CHL',152),('CN','CHINA','China','CHN',156),('CX','CHRISTMAS ISLAND','Christmas Island',NULL,NULL),('CC','COCOS (KEELING) ISLANDS','Cocos (Keeling) Islands',NULL,NULL),('CO','COLOMBIA','Colombia','COL',170),('KM','COMOROS','Comoros','COM',174),('CG','CONGO','Congo','COG',178),('CD','CONGO, THE DEMOCRATIC REPUBLIC OF THE','Congo, the Democratic Republic of the','COD',180),('CK','COOK ISLANDS','Cook Islands','COK',184),('CR','COSTA RICA','Costa Rica','CRI',188),('CI','COTE D\'IVOIRE','Cote D\'Ivoire','CIV',384),('HR','CROATIA','Croatia','HRV',191),('CU','CUBA','Cuba','CUB',192),('CY','CYPRUS','Cyprus','CYP',196),('CZ','CZECH REPUBLIC','Czech Republic','CZE',203),('DK','DENMARK','Denmark','DNK',208),('DJ','DJIBOUTI','Djibouti','DJI',262),('DM','DOMINICA','Dominica','DMA',212),('DO','DOMINICAN REPUBLIC','Dominican Republic','DOM',214),('EC','ECUADOR','Ecuador','ECU',218),('EG','EGYPT','Egypt','EGY',818),('SV','EL SALVADOR','El Salvador','SLV',222),('GQ','EQUATORIAL GUINEA','Equatorial Guinea','GNQ',226),('ER','ERITREA','Eritrea','ERI',232),('EE','ESTONIA','Estonia','EST',233),('ET','ETHIOPIA','Ethiopia','ETH',231),('FK','FALKLAND ISLANDS (MALVINAS)','Falkland Islands (Malvinas)','FLK',238),('FO','FAROE ISLANDS','Faroe Islands','FRO',234),('FJ','FIJI','Fiji','FJI',242),('FI','FINLAND','Finland','FIN',246),('FR','FRANCE','France','FRA',250),('GF','FRENCH GUIANA','French Guiana','GUF',254),('PF','FRENCH POLYNESIA','French Polynesia','PYF',258),('TF','FRENCH SOUTHERN TERRITORIES','French Southern Territories',NULL,NULL),('GA','GABON','Gabon','GAB',266),('GM','GAMBIA','Gambia','GMB',270),('GE','GEORGIA','Georgia','GEO',268),('DE','GERMANY','Germany','DEU',276),('GH','GHANA','Ghana','GHA',288),('GI','GIBRALTAR','Gibraltar','GIB',292),('GR','GREECE','Greece','GRC',300),('GL','GREENLAND','Greenland','GRL',304),('GD','GRENADA','Grenada','GRD',308),('GP','GUADELOUPE','Guadeloupe','GLP',312),('GU','GUAM','Guam','GUM',316),('GT','GUATEMALA','Guatemala','GTM',320),('GN','GUINEA','Guinea','GIN',324),('GW','GUINEA-BISSAU','Guinea-Bissau','GNB',624),('GY','GUYANA','Guyana','GUY',328),('HT','HAITI','Haiti','HTI',332),('HM','HEARD ISLAND AND MCDONALD ISLANDS','Heard Island and Mcdonald Islands',NULL,NULL),('VA','HOLY SEE (VATICAN CITY STATE)','Holy See (Vatican City State)','VAT',336),('HN','HONDURAS','Honduras','HND',340),('HK','HONG KONG','Hong Kong','HKG',344),('HU','HUNGARY','Hungary','HUN',348),('IS','ICELAND','Iceland','ISL',352),('IN','INDIA','India','IND',356),('ID','INDONESIA','Indonesia','IDN',360),('IR','IRAN, ISLAMIC REPUBLIC OF','Iran, Islamic Republic of','IRN',364),('IQ','IRAQ','Iraq','IRQ',368),('IE','IRELAND','Ireland','IRL',372),('IL','ISRAEL','Israel','ISR',376),('IT','ITALY','Italy','ITA',380),('JM','JAMAICA','Jamaica','JAM',388),('JP','JAPAN','Japan','JPN',392),('JO','JORDAN','Jordan','JOR',400),('KZ','KAZAKHSTAN','Kazakhstan','KAZ',398),('KE','KENYA','Kenya','KEN',404),('KI','KIRIBATI','Kiribati','KIR',296),('KP','KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF','Korea, Democratic People\'s Republic of','PRK',408),('KR','KOREA, REPUBLIC OF','Korea, Republic of','KOR',410),('KW','KUWAIT','Kuwait','KWT',414),('KG','KYRGYZSTAN','Kyrgyzstan','KGZ',417),('LA','LAO PEOPLE\'S DEMOCRATIC REPUBLIC','Lao People\'s Democratic Republic','LAO',418),('LV','LATVIA','Latvia','LVA',428),('LB','LEBANON','Lebanon','LBN',422),('LS','LESOTHO','Lesotho','LSO',426),('LR','LIBERIA','Liberia','LBR',430),('LY','LIBYAN ARAB JAMAHIRIYA','Libyan Arab Jamahiriya','LBY',434),('LI','LIECHTENSTEIN','Liechtenstein','LIE',438),('LT','LITHUANIA','Lithuania','LTU',440),('LU','LUXEMBOURG','Luxembourg','LUX',442),('MO','MACAO','Macao','MAC',446),('MK','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','Macedonia, the Former Yugoslav Republic of','MKD',807),('MG','MADAGASCAR','Madagascar','MDG',450),('MW','MALAWI','Malawi','MWI',454),('MY','MALAYSIA','Malaysia','MYS',458),('MV','MALDIVES','Maldives','MDV',462),('ML','MALI','Mali','MLI',466),('MT','MALTA','Malta','MLT',470),('MH','MARSHALL ISLANDS','Marshall Islands','MHL',584),('MQ','MARTINIQUE','Martinique','MTQ',474),('MR','MAURITANIA','Mauritania','MRT',478),('MU','MAURITIUS','Mauritius','MUS',480),('YT','MAYOTTE','Mayotte',NULL,NULL),('MX','MEXICO','Mexico','MEX',484),('FM','MICRONESIA, FEDERATED STATES OF','Micronesia, Federated States of','FSM',583),('MD','MOLDOVA, REPUBLIC OF','Moldova, Republic of','MDA',498),('MC','MONACO','Monaco','MCO',492),('MN','MONGOLIA','Mongolia','MNG',496),('MS','MONTSERRAT','Montserrat','MSR',500),('MA','MOROCCO','Morocco','MAR',504),('MZ','MOZAMBIQUE','Mozambique','MOZ',508),('MM','MYANMAR','Myanmar','MMR',104),('NA','NAMIBIA','Namibia','NAM',516),('NR','NAURU','Nauru','NRU',520),('NP','NEPAL','Nepal','NPL',524),('NL','NETHERLANDS','Netherlands','NLD',528),('AN','NETHERLANDS ANTILLES','Netherlands Antilles','ANT',530),('NC','NEW CALEDONIA','New Caledonia','NCL',540),('NZ','NEW ZEALAND','New Zealand','NZL',554),('NI','NICARAGUA','Nicaragua','NIC',558),('NE','NIGER','Niger','NER',562),('NG','NIGERIA','Nigeria','NGA',566),('NU','NIUE','Niue','NIU',570),('NF','NORFOLK ISLAND','Norfolk Island','NFK',574),('MP','NORTHERN MARIANA ISLANDS','Northern Mariana Islands','MNP',580),('NO','NORWAY','Norway','NOR',578),('OM','OMAN','Oman','OMN',512),('PK','PAKISTAN','Pakistan','PAK',586),('PW','PALAU','Palau','PLW',585),('PS','PALESTINIAN TERRITORY, OCCUPIED','Palestinian Territory, Occupied',NULL,NULL),('PA','PANAMA','Panama','PAN',591),('PG','PAPUA NEW GUINEA','Papua New Guinea','PNG',598),('PY','PARAGUAY','Paraguay','PRY',600),('PE','PERU','Peru','PER',604),('PH','PHILIPPINES','Philippines','PHL',608),('PN','PITCAIRN','Pitcairn','PCN',612),('PL','POLAND','Poland','POL',616),('PT','PORTUGAL','Portugal','PRT',620),('PR','PUERTO RICO','Puerto Rico','PRI',630),('QA','QATAR','Qatar','QAT',634),('RE','REUNION','Reunion','REU',638),('RO','ROMANIA','Romania','ROM',642),('RU','RUSSIAN FEDERATION','Russian Federation','RUS',643),('RW','RWANDA','Rwanda','RWA',646),('SH','SAINT HELENA','Saint Helena','SHN',654),('KN','SAINT KITTS AND NEVIS','Saint Kitts and Nevis','KNA',659),('LC','SAINT LUCIA','Saint Lucia','LCA',662),('PM','SAINT PIERRE AND MIQUELON','Saint Pierre and Miquelon','SPM',666),('VC','SAINT VINCENT AND THE GRENADINES','Saint Vincent and the Grenadines','VCT',670),('WS','SAMOA','Samoa','WSM',882),('SM','SAN MARINO','San Marino','SMR',674),('ST','SAO TOME AND PRINCIPE','Sao Tome and Principe','STP',678),('SA','SAUDI ARABIA','Saudi Arabia','SAU',682),('SN','SENEGAL','Senegal','SEN',686),('CS','SERBIA AND MONTENEGRO','Serbia and Montenegro',NULL,NULL),('SC','SEYCHELLES','Seychelles','SYC',690),('SL','SIERRA LEONE','Sierra Leone','SLE',694),('SG','SINGAPORE','Singapore','SGP',702),('SK','SLOVAKIA','Slovakia','SVK',703),('SI','SLOVENIA','Slovenia','SVN',705),('SB','SOLOMON ISLANDS','Solomon Islands','SLB',90),('SO','SOMALIA','Somalia','SOM',706),('ZA','SOUTH AFRICA','South Africa','ZAF',710),('GS','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','South Georgia and the South Sandwich Islands',NULL,NULL),('ES','SPAIN','Spain','ESP',724),('LK','SRI LANKA','Sri Lanka','LKA',144),('SD','SUDAN','Sudan','SDN',736),('SR','SURINAME','Suriname','SUR',740),('SJ','SVALBARD AND JAN MAYEN','Svalbard and Jan Mayen','SJM',744),('SZ','SWAZILAND','Swaziland','SWZ',748),('SE','SWEDEN','Sweden','SWE',752),('CH','SWITZERLAND','Switzerland','CHE',756),('SY','SYRIAN ARAB REPUBLIC','Syrian Arab Republic','SYR',760),('TW','TAIWAN, PROVINCE OF CHINA','Taiwan, Province of China','TWN',158),('TJ','TAJIKISTAN','Tajikistan','TJK',762),('TZ','TANZANIA, UNITED REPUBLIC OF','Tanzania, United Republic of','TZA',834),('TH','THAILAND','Thailand','THA',764),('TL','TIMOR-LESTE','Timor-Leste',NULL,NULL),('TG','TOGO','Togo','TGO',768),('TK','TOKELAU','Tokelau','TKL',772),('TO','TONGA','Tonga','TON',776),('TT','TRINIDAD AND TOBAGO','Trinidad and Tobago','TTO',780),('TN','TUNISIA','Tunisia','TUN',788),('TR','TURKEY','Turkey','TUR',792),('TM','TURKMENISTAN','Turkmenistan','TKM',795),('TC','TURKS AND CAICOS ISLANDS','Turks and Caicos Islands','TCA',796),('TV','TUVALU','Tuvalu','TUV',798),('UG','UGANDA','Uganda','UGA',800),('UA','UKRAINE','Ukraine','UKR',804),('AE','UNITED ARAB EMIRATES','United Arab Emirates','ARE',784),('GB','UNITED KINGDOM','United Kingdom','GBR',826),('US','UNITED STATES','United States','USA',840),('UM','UNITED STATES MINOR OUTLYING ISLANDS','United States Minor Outlying Islands',NULL,NULL),('UY','URUGUAY','Uruguay','URY',858),('UZ','UZBEKISTAN','Uzbekistan','UZB',860),('VU','VANUATU','Vanuatu','VUT',548),('VE','VENEZUELA','Venezuela','VEN',862),('VN','VIET NAM','Viet Nam','VNM',704),('VG','VIRGIN ISLANDS, BRITISH','Virgin Islands, British','VGB',92),('VI','VIRGIN ISLANDS, U.S.','Virgin Islands, U.s.','VIR',850),('WF','WALLIS AND FUTUNA','Wallis and Futuna','WLF',876),('EH','WESTERN SAHARA','Western Sahara','ESH',732),('YE','YEMEN','Yemen','YEM',887),('ZM','ZAMBIA','Zambia','ZMB',894),('ZW','ZIMBABWE','Zimbabwe','ZWE',716);
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `cron`
--

LOCK TABLES `cron` WRITE;
/*!40000 ALTER TABLE `cron` DISABLE KEYS */;
/*!40000 ALTER TABLE `cron` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `dns_rr`
--

LOCK TABLES `dns_rr` WRITE;
/*!40000 ALTER TABLE `dns_rr` DISABLE KEYS */;
/*!40000 ALTER TABLE `dns_rr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `dns_slave`
--

LOCK TABLES `dns_slave` WRITE;
/*!40000 ALTER TABLE `dns_slave` DISABLE KEYS */;
/*!40000 ALTER TABLE `dns_slave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `dns_soa`
--

LOCK TABLES `dns_soa` WRITE;
/*!40000 ALTER TABLE `dns_soa` DISABLE KEYS */;
/*!40000 ALTER TABLE `dns_soa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `dns_template`
--

LOCK TABLES `dns_template` WRITE;
/*!40000 ALTER TABLE `dns_template` DISABLE KEYS */;
INSERT INTO `dns_template` (`template_id`, `sys_userid`, `sys_groupid`, `sys_perm_user`, `sys_perm_group`, `sys_perm_other`, `name`, `fields`, `template`, `visible`) VALUES (1,1,1,'riud','riud','','Default','DOMAIN,IP,NS1,NS2,EMAIL','[ZONE]\norigin={DOMAIN}.\nns={NS1}.\nmbox={EMAIL}.\nrefresh=28800\nretry=7200\nexpire=604800\nminimum=86400\nttl=86400\n\n[DNS_RECORDS]\nA|{DOMAIN}.|{IP}|0|86400\nA|www|{IP}|0|86400\nA|mail|{IP}|0|86400\nNS|{DOMAIN}.|{NS1}.|0|86400\nNS|{DOMAIN}.|{NS2}.|0|86400\nMX|{DOMAIN}.|mail.{DOMAIN}.|10|86400','Y');
/*!40000 ALTER TABLE `dns_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `firewall`
--

LOCK TABLES `firewall` WRITE;
/*!40000 ALTER TABLE `firewall` DISABLE KEYS */;
/*!40000 ALTER TABLE `firewall` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ftp_user`
--

LOCK TABLES `ftp_user` WRITE;
/*!40000 ALTER TABLE `ftp_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `ftp_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mail_access`
--

LOCK TABLES `mail_access` WRITE;
/*!40000 ALTER TABLE `mail_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mail_content_filter`
--

LOCK TABLES `mail_content_filter` WRITE;
/*!40000 ALTER TABLE `mail_content_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_content_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mail_domain`
--

LOCK TABLES `mail_domain` WRITE;
/*!40000 ALTER TABLE `mail_domain` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mail_forwarding`
--

LOCK TABLES `mail_forwarding` WRITE;
/*!40000 ALTER TABLE `mail_forwarding` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_forwarding` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mail_get`
--

LOCK TABLES `mail_get` WRITE;
/*!40000 ALTER TABLE `mail_get` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_get` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mail_greylist`
--

LOCK TABLES `mail_greylist` WRITE;
/*!40000 ALTER TABLE `mail_greylist` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_greylist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mail_mailman_domain`
--

LOCK TABLES `mail_mailman_domain` WRITE;
/*!40000 ALTER TABLE `mail_mailman_domain` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_mailman_domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mail_relay_recipient`
--

LOCK TABLES `mail_relay_recipient` WRITE;
/*!40000 ALTER TABLE `mail_relay_recipient` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_relay_recipient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mail_traffic`
--

LOCK TABLES `mail_traffic` WRITE;
/*!40000 ALTER TABLE `mail_traffic` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_traffic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mail_transport`
--

LOCK TABLES `mail_transport` WRITE;
/*!40000 ALTER TABLE `mail_transport` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_transport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mail_user`
--

LOCK TABLES `mail_user` WRITE;
/*!40000 ALTER TABLE `mail_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mail_user_filter`
--

LOCK TABLES `mail_user_filter` WRITE;
/*!40000 ALTER TABLE `mail_user_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_user_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `monitor_data`
--

LOCK TABLES `monitor_data` WRITE;
/*!40000 ALTER TABLE `monitor_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `monitor_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `remote_session`
--

LOCK TABLES `remote_session` WRITE;
/*!40000 ALTER TABLE `remote_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `remote_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `remote_user`
--

LOCK TABLES `remote_user` WRITE;
/*!40000 ALTER TABLE `remote_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `remote_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `server`
--

LOCK TABLES `server` WRITE;
/*!40000 ALTER TABLE `server` DISABLE KEYS */;
INSERT INTO `server` (`server_id`, `sys_userid`, `sys_groupid`, `sys_perm_user`, `sys_perm_group`, `sys_perm_other`, `server_name`, `mail_server`, `web_server`, `dns_server`, `file_server`, `db_server`, `vserver_server`, `config`, `updated`, `mirror_server_id`, `dbversion`, `active`) VALUES (1,1,1,'riud','riud','r','troll-fedora',1,1,1,1,1,1,'[global]\nwebserver=apache\nmailserver=postfix\ndnsserver=mydns\n\n[server]\nauto_network_configuration=n\nip_address=192.168.0.16\nnetmask=255.255.255.0\ngateway=192.168.0.1\nhostname=troll-fedora\nnameservers=192.168.0.1,192.168.0.2\nloglevel=2\nbackup_dir=/var/backup\n\n[mail]\nmodule=postfix_mysql\nmaildir_path=/var/vmail/[domain]/[localpart]\nhomedir_path=/var/vmail\npop3_imap_daemon=courier\nmail_filter_syntax=maildrop\nmailuser_uid=5000\nmailuser_gid=5000\nmailuser_name=vmail\nmailuser_group=vmail\nrelayhost=\nrelayhost_user=\nrelayhost_password=\nmailbox_size_limit=0\nmessage_size_limit=0\n\n[getmail]\ngetmail_config_dir=/etc/getmail\n\n[web]\nwebsite_basedir=/var/www\nwebsite_path=/var/www/clients/client[client_id]/web[website_id]\nwebsite_symlinks=/var/www/[website_domain]/:/var/www/clients/client[client_id]/[website_domain]/\nvhost_conf_dir=/etc/httpd/conf/sites-available\nvhost_conf_enabled_dir=/etc/httpd/conf/sites-enabled\nsecurity_level=20\nuser=apache\ngroup=apache\napps_vhost_port=8081\napps_vhost_ip=_default_\napps_vhost_servername=\nphp_open_basedir=[website_path]/web:[website_path]/tmp:/var/www/[website_domain]/web:/srv/www/[website_domain]/web:/usr/share/php5:/tmp:/usr/share/phpmyadmin:/etc/phpmyadmin:/var/lib/phpmyadmin\nhtaccess_allow_override=All\nawstats_conf_dir=/etc/awstats\nawstats_data_dir=/var/lib/awstats\nawstats_pl=/usr/lib/cgi-bin/awstats.pl\nawstats_buildstaticpages_pl=/usr/share/awstats/tools/awstats_buildstaticpages.pl\nphp_ini_path_apache=/etc/php.ini\nphp_ini_path_cgi=/etc/php.ini\ncheck_apache_config=y\n\n[dns]\nbind_user=named\nbind_group=named\nbind_zonefiles_dir=/var/named\nnamed_conf_path=/etc/named.conf\nnamed_conf_local_path=/etc/named.conf.local\n\n[fastcgi]\nfastcgi_starter_path=/var/www/php-fcgi-scripts/[system_user]/\nfastcgi_starter_script=.php-fcgi-starter\nfastcgi_alias=/php/\nfastcgi_phpini_path=/etc/\nfastcgi_children=8\nfastcgi_max_requests=5000\nfastcgi_bin=/usr/bin/php-cgi\n\n[jailkit]\njailkit_chroot_home=/home/[username]\njailkit_chroot_app_sections=basicshell editors extendedshell netutils ssh sftp scp groups jk_lsh\njailkit_chroot_app_programs=/usr/bin/groups /usr/bin/id /usr/bin/dircolors /bin/basename /usr/bin/dirname /usr/bin/nano\njailkit_chroot_cron_programs=/usr/bin/php /usr/bin/perl /usr/share/perl /usr/share/php\n\n[vlogger]\nconfig_dir=/etc\n\n[cron]\ninit_script=cron\ncrontab_dir=/etc/cron.d\nwget=/usr/bin/wget\n\n',0,0,0,1);
/*!40000 ALTER TABLE `server` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `server_ip`
--

LOCK TABLES `server_ip` WRITE;
/*!40000 ALTER TABLE `server_ip` DISABLE KEYS */;
/*!40000 ALTER TABLE `server_ip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `shell_user`
--

LOCK TABLES `shell_user` WRITE;
/*!40000 ALTER TABLE `shell_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `shell_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `software_package`
--

LOCK TABLES `software_package` WRITE;
/*!40000 ALTER TABLE `software_package` DISABLE KEYS */;
/*!40000 ALTER TABLE `software_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `software_repo`
--

LOCK TABLES `software_repo` WRITE;
/*!40000 ALTER TABLE `software_repo` DISABLE KEYS */;
INSERT INTO `software_repo` (`software_repo_id`, `sys_userid`, `sys_groupid`, `sys_perm_user`, `sys_perm_group`, `sys_perm_other`, `repo_name`, `repo_url`, `repo_username`, `repo_password`, `active`) VALUES (1,1,1,'riud','riud','','ISPConfig Addons','http://repo.ispconfig.org/addons/','','','n');
/*!40000 ALTER TABLE `software_repo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `software_update`
--

LOCK TABLES `software_update` WRITE;
/*!40000 ALTER TABLE `software_update` DISABLE KEYS */;
/*!40000 ALTER TABLE `software_update` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `software_update_inst`
--

LOCK TABLES `software_update_inst` WRITE;
/*!40000 ALTER TABLE `software_update_inst` DISABLE KEYS */;
/*!40000 ALTER TABLE `software_update_inst` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `spamfilter_policy`
--

LOCK TABLES `spamfilter_policy` WRITE;
/*!40000 ALTER TABLE `spamfilter_policy` DISABLE KEYS */;
INSERT INTO `spamfilter_policy` (`id`, `sys_userid`, `sys_groupid`, `sys_perm_user`, `sys_perm_group`, `sys_perm_other`, `policy_name`, `virus_lover`, `spam_lover`, `banned_files_lover`, `bad_header_lover`, `bypass_virus_checks`, `bypass_spam_checks`, `bypass_banned_checks`, `bypass_header_checks`, `spam_modifies_subj`, `virus_quarantine_to`, `spam_quarantine_to`, `banned_quarantine_to`, `bad_header_quarantine_to`, `clean_quarantine_to`, `other_quarantine_to`, `spam_tag_level`, `spam_tag2_level`, `spam_kill_level`, `spam_dsn_cutoff_level`, `spam_quarantine_cutoff_level`, `addr_extension_virus`, `addr_extension_spam`, `addr_extension_banned`, `addr_extension_bad_header`, `warnvirusrecip`, `warnbannedrecip`, `warnbadhrecip`, `newvirus_admin`, `virus_admin`, `banned_admin`, `bad_header_admin`, `spam_admin`, `spam_subject_tag`, `spam_subject_tag2`, `message_size_limit`, `banned_rulenames`) VALUES (1,1,0,'riud','riud','r','Non-paying','N','N','N','N','Y','Y','Y','N','Y','','','','','','',3,7,10,0,0,'','','','','N','N','N','','','','','','','',0,''),(2,1,0,'riud','riud','r','Uncensored','Y','Y','Y','Y','N','N','N','N','N',NULL,NULL,NULL,NULL,NULL,NULL,3,999,999,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,1,0,'riud','riud','r','Wants all spam','N','Y','N','N','N','N','N','N','Y',NULL,NULL,NULL,NULL,NULL,NULL,3,999,999,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,1,0,'riud','riud','r','Wants viruses','Y','N','Y','Y','N','N','N','N','Y',NULL,NULL,NULL,NULL,NULL,NULL,3,6.9,6.9,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,1,0,'riud','riud','r','Normal','N','N','N','N','N','N','N','N','Y','','','','','','',1,4.5,50,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','***SPAM***',NULL,NULL),(6,1,0,'riud','riud','r','Trigger happy','N','N','N','N','N','N','N','N','Y',NULL,NULL,NULL,NULL,NULL,NULL,3,5,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,1,0,'riud','riud','r','Permissive','N','N','N','Y','N','N','N','N','Y',NULL,NULL,NULL,NULL,NULL,NULL,3,10,20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `spamfilter_policy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `spamfilter_users`
--

LOCK TABLES `spamfilter_users` WRITE;
/*!40000 ALTER TABLE `spamfilter_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `spamfilter_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `spamfilter_wblist`
--

LOCK TABLES `spamfilter_wblist` WRITE;
/*!40000 ALTER TABLE `spamfilter_wblist` DISABLE KEYS */;
/*!40000 ALTER TABLE `spamfilter_wblist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `support_message`
--

LOCK TABLES `support_message` WRITE;
/*!40000 ALTER TABLE `support_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `support_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sys_config`
--

LOCK TABLES `sys_config` WRITE;
/*!40000 ALTER TABLE `sys_config` DISABLE KEYS */;
INSERT INTO `sys_config` (`config_id`, `group`, `name`, `value`) VALUES (1,'db','db_version','3.0.3');
/*!40000 ALTER TABLE `sys_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sys_datalog`
--

LOCK TABLES `sys_datalog` WRITE;
/*!40000 ALTER TABLE `sys_datalog` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_datalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sys_dbsync`
--

LOCK TABLES `sys_dbsync` WRITE;
/*!40000 ALTER TABLE `sys_dbsync` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_dbsync` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sys_filesync`
--

LOCK TABLES `sys_filesync` WRITE;
/*!40000 ALTER TABLE `sys_filesync` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_filesync` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sys_group`
--

LOCK TABLES `sys_group` WRITE;
/*!40000 ALTER TABLE `sys_group` DISABLE KEYS */;
INSERT INTO `sys_group` (`groupid`, `name`, `description`, `client_id`) VALUES (1,'admin','Administrators group',0);
/*!40000 ALTER TABLE `sys_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sys_ini`
--

LOCK TABLES `sys_ini` WRITE;
/*!40000 ALTER TABLE `sys_ini` DISABLE KEYS */;
INSERT INTO `sys_ini` (`sysini_id`, `config`) VALUES (1,'[global]\n\n[admin]\n\n[client]\n\n[dns]\n\n[mail]\nmailboxlist_webmail_link=y\nwebmail_url=\n\n[monitor]\n\n[sites]\ndbname_prefix=c[CLIENTID]\ndbuser_prefix=c[CLIENTID]\nftpuser_prefix=[CLIENTNAME]\nshelluser_prefix=[CLIENTNAME]\nwebdavuser_prefix=[CLIENTNAME]\ndblist_phpmyadmin_link=y\nphpmyadmin_url=\nwebftp_url=\n\n[tools]\n\n[domains]\nuse_domain_module=n\nnew_domain_html=Please contact our support to create a new domain for you.\n\n[misc]\ndashboard_atom_url=http://www.ispconfig.org/atom\n');
/*!40000 ALTER TABLE `sys_ini` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sys_log`
--

LOCK TABLES `sys_log` WRITE;
/*!40000 ALTER TABLE `sys_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sys_remoteaction`
--

LOCK TABLES `sys_remoteaction` WRITE;
/*!40000 ALTER TABLE `sys_remoteaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_remoteaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` (`userid`, `sys_userid`, `sys_groupid`, `sys_perm_user`, `sys_perm_group`, `sys_perm_other`, `username`, `passwort`, `modules`, `startmodule`, `app_theme`, `typ`, `active`, `language`, `groups`, `default_group`, `client_id`) VALUES (1,1,0,'riud','riud','','admin','21232f297a57a5a743894a0e4a801fc3','admin,client,mail,monitor,sites,dns,tools,help','mail','default','admin',1,'en','1,2',1,0);
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `web_database`
--

LOCK TABLES `web_database` WRITE;
/*!40000 ALTER TABLE `web_database` DISABLE KEYS */;
/*!40000 ALTER TABLE `web_database` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `web_domain`
--

LOCK TABLES `web_domain` WRITE;
/*!40000 ALTER TABLE `web_domain` DISABLE KEYS */;
/*!40000 ALTER TABLE `web_domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `web_traffic`
--

LOCK TABLES `web_traffic` WRITE;
/*!40000 ALTER TABLE `web_traffic` DISABLE KEYS */;
/*!40000 ALTER TABLE `web_traffic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `webdav_user`
--

LOCK TABLES `webdav_user` WRITE;
/*!40000 ALTER TABLE `webdav_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `webdav_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2010-11-27 18:52:01
